package com.zzh.lib.def;


import com.zzh.lib.video.cache.preload.PreMultiLoadTask;

import java.io.InputStream;

/**
 * Created by ZZH on 2022/8/15.
 *
 * @Date: 2022/8/15
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description: 文件缓存处理
 */
public interface PreCacheFileProvider {
    public void onCacheFile(PreMultiLoadTask task, InputStream inputStream);
}
