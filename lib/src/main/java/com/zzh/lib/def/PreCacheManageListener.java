package com.zzh.lib.def;


import com.zzh.lib.video.cache.preload.PreMultiLoadTask;

/**
 * Created by ZZH on 2022/8/15.
 *
 * @Date: 2022/8/15
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description: 预缓存监听。
 */
public interface PreCacheManageListener {

    void onCacheComplete(PreMultiLoadTask task);
}
