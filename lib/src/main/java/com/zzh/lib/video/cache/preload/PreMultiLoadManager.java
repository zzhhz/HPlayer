package com.zzh.lib.video.cache.preload;


import android.content.Context;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;

import com.zzh.lib.video.cache.HttpProxyCacheServer;
import com.zzh.lib.video.cache.file.Md5FileNameGenerator;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * 预加载管理
 *
 * @author zzh
 */
public class PreMultiLoadManager {
    /**
     * 预加载的大小，每个视频预加载10M，这个参数可根据实际情况调整
     */
    public static final int PRELOAD_LENGTH = 512 * 1024 * 2 * 10;

    public static final String TAG = PreMultiLoadManager.class.getSimpleName();
    private int cacheVideoCount = 10;


    public ArrayMap<String, VideoPreMultiLoadFuture> videoPreLoadFutureArrayMap = new ArrayMap<>();
    public List<PreMultiLoadTask> mPreMultiLoadTaskPool = new ArrayList<>();

    public HttpProxyCacheServer httpProxyCacheServer;
    public Md5FileNameGenerator fileNameGenerator;
    public Context context;

    private static volatile PreMultiLoadManager sInstance;


    public int getCacheVideoCount() {
        return cacheVideoCount;
    }

    public void setCacheVideoCount(int cacheVideoCount) {
        this.cacheVideoCount = cacheVideoCount;
    }

    private PreMultiLoadManager(Context context) {
        httpProxyCacheServer = PlayerEnvironment.getProxy(context);
        fileNameGenerator = new Md5FileNameGenerator();
        this.context = context;
    }

    /**
     * 必须先调用 getInstance(Context context)
     *
     * @return sInstance
     */
    public static PreMultiLoadManager getInstance() {
        return sInstance;
    }

    public static PreMultiLoadManager getInstance(Context context) {
        if (sInstance == null) {
            synchronized (PreMultiLoadManager.class) {
                if (sInstance == null) {
                    sInstance = new PreMultiLoadManager(context);
                }
            }
        }

        return sInstance;
    }


    protected void putFuture(String busId, VideoPreMultiLoadFuture videoPreMultiLoadFuture) {
        videoPreLoadFutureArrayMap.put(busId, videoPreMultiLoadFuture);
    }

    protected void removeFuture(String busId) {
        videoPreLoadFutureArrayMap.remove(busId);
    }

    public VideoPreMultiLoadFuture getVideoPreLoadFuture(String busId) {
        return videoPreLoadFutureArrayMap.get(busId);
    }

    public VideoPreMultiLoadFuture getOrCreateVideoPreLoadFuture(String busId) {
        VideoPreMultiLoadFuture videoPreMultiLoadFuture = getVideoPreLoadFuture(busId);
        synchronized (PreMultiLoadManager.class) {
            if (videoPreMultiLoadFuture == null) {
                videoPreMultiLoadFuture = new VideoPreMultiLoadFuture(context, busId);
                putFuture(busId, videoPreMultiLoadFuture);
            }
        }
        return videoPreMultiLoadFuture;
    }


    public void currentVideoPlay(String busId, String url) {
        if (TextUtils.isEmpty(busId) || TextUtils.isEmpty(url)) {
            return;
        }
        Log.d(TAG, "-----当前播放地址：" + busId + ", " + url);

        VideoPreMultiLoadFuture videoPreMultiLoadFuture = getVideoPreLoadFuture(busId);

        if (videoPreMultiLoadFuture != null) {
            videoPreMultiLoadFuture.currentPlayUrl(url);
        }
    }

    public boolean hasEnoughCache(String url) {
        return AndroidUtils.hasEnoughCache(context, fileNameGenerator, url);
    }

    protected synchronized PreMultiLoadTask createTask(final String busId, String url, int index) {
        PreMultiLoadTask preMultiLoadTask = null;
        if (mPreMultiLoadTaskPool.size() > 0) {
            preMultiLoadTask = mPreMultiLoadTaskPool.get(0);
            mPreMultiLoadTaskPool.remove(0);
            if (AndroidUtils.isDebug()) {
                Log.d(TAG, "get PreLoadTask from pool");
            }
        }

        if (preMultiLoadTask == null) {
            preMultiLoadTask = new PreMultiLoadTask(context, url, index);
            if (AndroidUtils.isDebug()) {
                Log.d(TAG, "new PreLoadTask");
            }

            final PreMultiLoadTask tmpPreMultiLoadTask = preMultiLoadTask;
            preMultiLoadTask.setTaskCallback(() -> {
                VideoPreMultiLoadFuture videoPreMultiLoadFuture = getVideoPreLoadFuture(busId);
                if (videoPreMultiLoadFuture != null) {
                    videoPreMultiLoadFuture.removeTask(tmpPreMultiLoadTask);
                }
                recyclerPreLoadTask(tmpPreMultiLoadTask);
            });
        } else {
            preMultiLoadTask.init(url, index);
        }

        return preMultiLoadTask;
    }

    protected synchronized void recyclerPreLoadTask(PreMultiLoadTask task) {
        if (mPreMultiLoadTaskPool.size() <= 20) {
            if (AndroidUtils.isDebug()) {
                Log.d(TAG, "recycler PreLoadTask into pool");
            }
            mPreMultiLoadTaskPool.add(task);
        }
    }

    protected String getLocalUrlAppendWithUrl(String url) {
        if (httpProxyCacheServer != null) {
            return httpProxyCacheServer.getProxyUrl(url);
        }
        return url;
    }

    /**
     * 进行文件缓存
     *
     * @param mCacheServer
     * @param mRawUrl
     * @param mPosition
     * @param mIsCanceled
     */
    public static void netCacheFile(HttpProxyCacheServer mCacheServer, String mRawUrl, int mPosition, boolean mIsCanceled) {
        AndroidUtils.d("开始预加载：" + mPosition);
        HttpURLConnection connection = null;
        try {
            //获取HttpProxyCacheServer的代理地址
            String proxyUrl = mCacheServer.getProxyUrl(mRawUrl);
            URL url = new URL(proxyUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(5_000);
            connection.setReadTimeout(5_000);
            InputStream in = new BufferedInputStream(connection.getInputStream());
            int length;
            int read = -1;
            byte[] bytes = new byte[8 * 1024];
            while ((length = in.read(bytes)) != -1) {
                read += length;
                //预加载完成或者取消预加载
                if (mIsCanceled || read >= PreloadManager.PRELOAD_LENGTH) {
                    AndroidUtils.d("结束预加载：" + mPosition);
                    connection.disconnect();
                    break;
                }
            }

        } catch (Exception e) {
            AndroidUtils.d("异常结束预加载：" + mPosition);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}


