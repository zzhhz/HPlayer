package com.zzh.lib.video.cache.preload;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;


import com.zzh.lib.video.cache.adapter.DefaultNetworkAdapter;
import com.zzh.lib.video.cache.adapter.INetworkAdapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;

/**
 * Created by ZZH on 2022/9/5
 *
 * @Date: 2022/9/5 17:46
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description: 缓存业务处理
 */
public class VideoPreMultiLoadFuture implements LifecycleObserver {

    public static final String TAG = "VideoPreLoadFuture";

    private volatile List<String> mUrls;
    private final String mBusId;
    private String mCurrentUrl;
    private volatile int mCurrentIndex;
    private volatile boolean toPreLoad = false;
    private final ReentrantLock mLock = new ReentrantLock();
    private final Condition empty = mLock.newCondition();
    private final Condition network = mLock.newCondition();
    private final LinkedBlockingDeque<PreMultiLoadTask> mLoadingTaskDeque = new LinkedBlockingDeque<>();
    private final ExecutorService mExecutorService = Executors.newCachedThreadPool();
    private ConsumerThread mConsumerThread;
    private CurrentLoadingHandler mHandler;
    private Context mContext;
    private INetworkAdapter mNetworkAdapter;
    private BroadcastReceiver mNetworkReceiver;
    private volatile boolean mIsWifi = false;

    /**
     * @param context
     * @param preloadBusId 每个页面对应一个busId
     */
    public VideoPreMultiLoadFuture(Context context, String preloadBusId) {
        mContext = context;
        mHandler = new CurrentLoadingHandler(this);

        if (context instanceof Application) {
            throw new RuntimeException("context should not be an Application");
        }

        if (context instanceof LifecycleOwner) {
            ((LifecycleOwner) context).getLifecycle().addObserver(this);
        }

        if (TextUtils.isEmpty(preloadBusId)) {
            throw new RuntimeException("busId should not be empty");
        }

        this.mBusId = preloadBusId;

        PreMultiLoadManager.getInstance(context).putFuture(mBusId, this);

        setNetworkAdapter(new DefaultNetworkAdapter());

        if (mNetworkReceiver == null) {
            mNetworkReceiver = new NetworkBroadcastReceiver();
        }

        if (mContext != null) {
            try {
                mContext.registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            } catch (Exception e) {
                AndroidUtils.e(TAG, this + "\tregisterReceiver exp:" + e);
            }
        }

        AndroidUtils.d(TAG, "ConsumerThread 启动缓存 start");
        mConsumerThread = new ConsumerThread();
        mConsumerThread.start();
    }

    public void setNetworkAdapter(INetworkAdapter networkAdapter) {
        mNetworkAdapter = networkAdapter;
    }

    public void addUrls(List<String> urls) {
        mLock.lock();
        try {
            if (this.mUrls == null) {
                this.mUrls = new ArrayList<>();
            }

            this.mUrls.addAll(urls);
            AndroidUtils.d(TAG, mBusId + ", 增加url缓存视频数据大小 addUrls: " + mUrls.size() + ", toPreLoad: " + toPreLoad + "， " + mLoadingTaskDeque.size());
            toPreLoad = true;
            if (mConsumerThread != null && mConsumerThread.isInterrupted()) {
                mConsumerThread.start();
            } else {
                empty.signal();
            }
        } finally {
            mLock.unlock();
        }
    }

    public void updateUrls(List<String> urls) {
        mLock.lock();
        try {
            if (this.mUrls != null) {
                this.mUrls.clear();
                this.mUrls.addAll(urls);
            } else {
                this.mUrls = urls;
            }
            toPreLoad = true;
            mCurrentIndex = 0;

            AndroidUtils.d("main", "增加url缓存视频数据大小 updateUrls: " + mUrls.size() + "， " + mLoadingTaskDeque.size());
        } finally {
            mLock.unlock();
        }
    }

    private boolean hasPause = false;

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void onPause() {
        /**
         * 线程进入阻塞
         * */
        AndroidUtils.d(TAG, "onPause: ");

        if (mNetworkReceiver != null) {
            try {
                if (mContext != null) {
                    mContext.unregisterReceiver(mNetworkReceiver);
                }
            } catch (Exception e) {
                AndroidUtils.e(TAG, this + "\tunregisterReceiver exp:" + e);
            }
        }

        mLock.lock();
        hasPause = true;
        try {
            PreMultiLoadTask task;
            while ((task = mLoadingTaskDeque.poll()) != null) {
                task.setStatus(PreMultiLoadTask.STATUS_CANCEL);
            }
        } catch (Exception e) {
            AndroidUtils.e(TAG, "onPause: " + e.getMessage());
        } finally {
            mLock.unlock();
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onResume() {
        /**
         * 唤醒进入阻塞的线程
         * */
        AndroidUtils.d(TAG, "onResume: ");

        if (mNetworkReceiver == null) {
            mNetworkReceiver = new NetworkBroadcastReceiver();
        }

        if (mContext != null) {
            try {
                mContext.registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            } catch (Exception e) {
                AndroidUtils.e(TAG, this + "\tregisterReceiver exp:" + e);
            }
        }

        mLock.lock();
        try {
            if (hasPause && !TextUtils.isEmpty(mCurrentUrl)) {
                toPreLoad = true;
                hasPause = false;
                AndroidUtils.d(TAG, "ConsumerThread is notified empty.signal()");
                empty.signal();
            }
        } catch (Exception e) {
            AndroidUtils.e(TAG, "onResume: " + e.getMessage());
        } finally {
            mLock.unlock();
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        AndroidUtils.d(TAG, "onDestroy: ");
        PreMultiLoadManager.getInstance(mContext).removeFuture(mBusId);
        /**
         * 关闭线程
         * */
        if (mConsumerThread != null && !mConsumerThread.isInterrupted()) {
            mLock.lock();
            try {
                mConsumerThread.interrupt();
                mCurrentIndex = -1;
                AndroidUtils.d(TAG, "onDestroy empty.signal()");
                empty.signal();
                PreMultiLoadTask task;
                while ((task = mLoadingTaskDeque.poll()) != null) {
                    task.setStatus(PreMultiLoadTask.STATUS_CANCEL);
                }
            } catch (Exception e) {
                AndroidUtils.e(TAG, "onDestroy: " + e.getMessage());
            } finally {
                mLock.unlock();
            }
        }
    }

    public void currentPlayUrl(String url) {
        mHandler.removeMessages(CurrentLoadingHandler.MSG_SET);
        Message message = Message.obtain();
        message.what = CurrentLoadingHandler.MSG_SET;
        message.obj = url;
        mHandler.sendMessage(message);
    }

    private void innerCurrentPlayUrl(String url) {
        mLock.lock();
        try {
            if (mUrls == null || mUrls.size() <= 0) {
                throw new RuntimeException("url list should not be empty");
            }

            if (!mUrls.contains(url)) {
                return;
            }

            mCurrentUrl = url;
            int currentIndex = mUrls.indexOf(url);
            if (currentIndex != -1 && currentIndex != mCurrentIndex) {
                AndroidUtils.d(TAG, "currentPlayUrl: [url: " + url + ", index: " + currentIndex + "]");
                mCurrentIndex = currentIndex;
                toPreLoad = true;
                // notify
                AndroidUtils.d(TAG, "ConsumerThread is notified empty.signal()");
                empty.signal();
            } else {
                AndroidUtils.d(TAG, "ConsumerThread 播放位置相等，不需要切换");
            }
        } catch (Exception e) {
            AndroidUtils.e(TAG, "currentPlayUrl: " + e.getMessage());
        } finally {
            mLock.unlock();
        }
    }

    public List<String> getUrls() {
        return mUrls == null ? new ArrayList<>() : mUrls;
    }

    private boolean isNetWorkConnect() {
        if (mContext == null) {
            return false;
        }
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        boolean isConnect = (netInfo != null && netInfo.isConnected());
        return isConnect;
    }

    class ConsumerThread extends Thread {

        @Override
        public void run() {
            mLock.lock();
            try {
                while (!isInterrupted()) {
                    AndroidUtils.i("-----i", "一直在运行着");
                    if (!isNetWorkConnect() || (!mIsWifi && !mNetworkAdapter.canPreLoadIfNotWifi())) {
                        AndroidUtils.d(TAG, "ConsumerThread is await for" + (isNetWorkConnect() ? " is not wifi " : " network not connect"));
                        network.await();
                    }

                    if (!toPreLoad) {
                        AndroidUtils.d(TAG, "ConsumerThread is await");
                        empty.await();
                    }

                    if (mCurrentIndex == -1) {
                        continue;
                    }
                    /**
                     * 默认加入队列为
                     * 【max(mCurrentIndex - 3, 0)， min(mCurrentIndex + 4, mUrls.size()-1 )]
                     * 0， 4
                     * */
                    AndroidUtils.d(TAG, "Consumer thread current index is: " + mCurrentIndex);
                    int firstIndex = Math.max(0, mCurrentIndex - 3);
                    int lastIndex = Math.min(mCurrentIndex + PreMultiLoadManager.getInstance().getCacheVideoCount() - 1, mUrls.size() - 1);
                    PreMultiLoadTask preMultiLoadTask = null;
                    String url;
                    for (int i = firstIndex; i <= lastIndex; i++) {
                        if (i == mCurrentIndex) {
                            continue;
                        }
                        url = mUrls.get(i);
                        if (TextUtils.isEmpty(url)) {
                            continue;
                        }
                        preMultiLoadTask = PreMultiLoadManager.getInstance(mContext).createTask(mBusId, url, i);
                        if (!mLoadingTaskDeque.contains(preMultiLoadTask)) {
                            if (mLoadingTaskDeque.size() >= PreMultiLoadManager.getInstance().getCacheVideoCount()) {
                                PreMultiLoadTask ingPreMultiLoadTask = mLoadingTaskDeque.pollLast();
                                ingPreMultiLoadTask.setStatus(PreMultiLoadTask.STATUS_CANCEL);
                                AndroidUtils.d(TAG, "mLoadingTaskDeque size more than 5, remove index: " + ingPreMultiLoadTask.index);
                            }
                            AndroidUtils.d(TAG, "Put into mLoadingTaskDeque: " + preMultiLoadTask.url);
                            mLoadingTaskDeque.addFirst(preMultiLoadTask);
                            mExecutorService.submit(preMultiLoadTask);
                        } else {
                            mLoadingTaskDeque.remove(preMultiLoadTask);
                            mLoadingTaskDeque.addFirst(preMultiLoadTask);
                        }
                    }

                    toPreLoad = false;
                }
            } catch (InterruptedException e) {
                AndroidUtils.e("-------", "抛出异常，中断缓存");
            } finally {
                AndroidUtils.d(TAG, "ConsumerThread is finish");
                mLock.unlock();
            }
        }

        @Override
        public boolean isInterrupted() {
            AndroidUtils.e("-------", "调用了 isInterrupted");
            return super.isInterrupted();
        }
    }

    public void removeTask(PreMultiLoadTask task) {
        mLock.lock();
        try {
            boolean flag = mLoadingTaskDeque.remove(task);
            AndroidUtils.d(TAG, "removeTask " + (flag ? "success" : "fail"));
        } finally {
            mLock.unlock();
        }
    }

    public class NetworkBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            boolean isConnect = (netInfo != null && netInfo.isConnected());
            if (isConnect) {
                int networkType = netInfo.getType();
                if (networkType == ConnectivityManager.TYPE_WIFI) {
                    mLock.lock();
                    try {
                        mIsWifi = true;
                        network.signal();
                    } finally {
                        mLock.unlock();
                    }
                } else {
                    mIsWifi = false;
                }
            } else {
                mIsWifi = false;
            }
        }
    }


    private static class CurrentLoadingHandler extends Handler {

        private static final int MSG_SET = 100;

        private WeakReference<VideoPreMultiLoadFuture> videoPreLoadFutureWeakReference;

        public CurrentLoadingHandler(VideoPreMultiLoadFuture videoPreMultiLoadFuture) {
            videoPreLoadFutureWeakReference = new WeakReference<>(videoPreMultiLoadFuture);
        }

        @Override
        public void handleMessage(Message msg) {

            VideoPreMultiLoadFuture videoPreMultiLoadFuture = videoPreLoadFutureWeakReference.get();

            if (videoPreMultiLoadFuture == null) {
                return;
            }

            switch (msg.what) {
                case MSG_SET:
                    if (msg.obj instanceof String) {
                        videoPreMultiLoadFuture.innerCurrentPlayUrl((String) msg.obj);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}

