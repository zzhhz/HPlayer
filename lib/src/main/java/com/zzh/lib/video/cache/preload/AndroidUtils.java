package com.zzh.lib.video.cache.preload;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.zzh.lib.video.cache.StorageUtils;
import com.zzh.lib.video.cache.file.Md5FileNameGenerator;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author zhipeng.zhuo
 * @date 2020-05-14
 */
public class AndroidUtils {
    private static boolean isDebug = false;

    public static boolean isDebug() {
        return isDebug;
    }

    public static String textToMD5(String plainText) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte[] b = md.digest();

            int i;

            StringBuilder buf = new StringBuilder();
            for (final byte b1 : b) {
                i = b1;
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            return buf.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }

    }

    public static final String TEMP_POSTFIX = ".download";

    public static boolean hasEnoughCache(Context context, Md5FileNameGenerator generator, String url) {
        try {
            File cacheRoot = StorageUtils.getIndividualCacheDirectory(context);
            String path = cacheRoot.getAbsolutePath();

            String name = generator.generate(url);
            if (TextUtils.isEmpty(name)) {
                AndroidUtils.d("判断缓存文件", "1不存在");
                return false;
            }
            File file = new File(path, name);

            if (file.exists() && file.canRead() && file.length() > 1024) {
                AndroidUtils.d("判断缓存文件", "是否存在1" + file.getAbsolutePath());
                return true;
            }

            file = new File(path, name + TEMP_POSTFIX);

            if (file.exists() && file.canRead() && file.length() > 102400) {
                AndroidUtils.d("判断缓存文件", "是否存在2" + file.getAbsolutePath());
                return true;
            }
        } catch (Throwable e) {
            AndroidUtils.d("判断缓存文件", "3不存在");
        }
        AndroidUtils.d("判断缓存文件", "2不存在");
        return false;
    }

    public static String appendUrl(String url, String cacheKey) {
        StringBuilder appendQuery = new StringBuilder(100);
        return AndroidUtils.appendUri(url, appendQuery);
    }

    public static String appendUri(String uri, StringBuilder appendQuery) {
        String result = uri;
        try {
            Uri olduri = Uri.parse(uri);
            String newQuery = olduri.getEncodedQuery();
            if (TextUtils.isEmpty(newQuery)) {
                newQuery = appendQuery.toString();
            } else {
                newQuery = appendQuery + "&" + newQuery;
            }
            // <scheme>://<authority><absolute path>?<query>#<fragment>
            Uri.Builder builder = new Uri.Builder();
            builder.scheme(olduri.getScheme()).encodedAuthority(olduri.getEncodedAuthority()).encodedPath(olduri.getEncodedPath()).encodedQuery(newQuery).fragment(olduri.getEncodedFragment());
            result = builder.build().toString();
        } catch (Exception e) {

        }
        return result;
    }

    public static void d(String tag, String msg) {
        if (isDebug()) {
            Log.d(tag, msg);
        }
    }

    public static void d(String msg) {
        d("-------", msg);
    }

    public static void i(String tag, String msg) {
        if (isDebug()) {
            Log.i(tag, msg);
        }
    }

    public static void i(String msg) {
        i("-------", msg);
    }

    public static void e(String tag, String msg) {
        if (isDebug()) {
            Log.e(tag, msg);
        }
    }

    public static void e(String msg) {
        e("-------", msg);
    }
}
