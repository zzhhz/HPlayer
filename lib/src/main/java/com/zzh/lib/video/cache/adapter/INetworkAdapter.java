package com.zzh.lib.video.cache.adapter;

/**
 * @author zhipeng.zhuo
 * @date 2020-06-18
 */
public interface INetworkAdapter {
    boolean canPreLoadIfNotWifi();
}
