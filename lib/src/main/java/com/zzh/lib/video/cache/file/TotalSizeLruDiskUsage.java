package com.zzh.lib.video.cache.file;


import java.io.File;


/**
 * {@link DiskUsage} that uses LRU (Least Recently Used) strategy and trims cache size to max size if needed.
 *
 * @author Alexey Danilov (danikula@gmail.com).
 */
public class TotalSizeLruDiskUsage extends LruDiskUsage {
    private final long maxSize;

    public TotalSizeLruDiskUsage(long maxSize) {
        if (maxSize <= 0) {
            this.maxSize = Long.MAX_VALUE;
        } else {
            this.maxSize = maxSize;
        }
    }

    @Override
    protected boolean accept(File file, long totalSize, int totalCount) {
        return totalSize <= maxSize;
    }
}
