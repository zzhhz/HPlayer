package com.zzh.lib.video.cache.preload;

import android.content.Context;


import com.zzh.lib.video.cache.HttpProxyCacheServer;
import com.zzh.lib.video.cache.StorageUtils;

import java.io.File;

@Deprecated
public class ProxyVideoCacheManager {

    private static HttpProxyCacheServer sharedProxy;

    private ProxyVideoCacheManager() {
    }

    public static HttpProxyCacheServer getProxy(Context context) {
        return sharedProxy == null ? (sharedProxy = newProxy(context)) : sharedProxy;
    }

    private static HttpProxyCacheServer newProxy(Context context) {
        return PlayerEnvironment.getProxy(context);
    }
}