package com.zzh.mediaplayer;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.zzh.lib.player.HMediaPlayer;
import com.zzh.lib.video.cache.preload.PreMultiLoadManager;
import com.zzh.lib.video.cache.preload.PreloadManager;
import com.zzh.lib.video.cache.preload.ProxyVideoCacheManager;


public class MainActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "MainActivity";

    private SurfaceView sfv_media;

    private final HMediaPlayer mPlayer = new HMediaPlayer();

    private Button btn_start, btn_pause, btn_stop, btn_reset, btn_play_pause, btn_play_stop;
    private TextView tv_duration;
    private SeekBar sb_progress;

    private final Handler mLooper = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sfv_media = findViewById(R.id.sfv_media);
        sb_progress = findViewById(R.id.sb_progress);
        tv_duration = findViewById(R.id.tv_duration);
        btn_start = findViewById(R.id.btn_start);
        btn_pause = findViewById(R.id.btn_pause);
        btn_stop = findViewById(R.id.btn_stop);
        btn_reset = findViewById(R.id.btn_reset);
        btn_play_pause = findViewById(R.id.btn_play_pause);
        btn_play_stop = findViewById(R.id.btn_play_stop);
        btn_start.setOnClickListener(this);
        btn_pause.setOnClickListener(this);
        btn_stop.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        btn_play_pause.setOnClickListener(this);
        btn_play_stop.setOnClickListener(this);

        sb_progress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sfv_media.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                mPlayer.setDisplay(holder);
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
            }
        });

        mPlayer.setOnExceptionCallback(new HMediaPlayer.OnExceptionCallback() {
            @Override
            public void onException(Exception e) {
                Log.i(TAG, "onException:" + e);
            }
        });
        mPlayer.addOnStateChangeCallback(new HMediaPlayer.OnStateChangeCallback() {
            @Override
            public void onStateChanged(HMediaPlayer player, HMediaPlayer.State oldState, HMediaPlayer.State newState) {

            }
        });
    }

    private final Runnable mPlayTimeRunnable = new Runnable() {
        @Override
        public void run() {
            final int currentPosition = mPlayer.getCurrentPosition();
            final int totalDuration = mPlayer.getDuration();

            sb_progress.setMax(totalDuration);
            sb_progress.setProgress(currentPosition);

            final String total = SDDateUtil.formatDuring2hhmmss(totalDuration);
            final String current = SDDateUtil.formatDuring2hhmmss(currentPosition);
            tv_duration.setText(current + "/" + total);
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start:
                mPlayer.setDataRawResId(R.raw.cbg, this); //设置要播放的数据
                mPlayer.start(); //播放
                break;
            case R.id.btn_pause:
                mPlayer.pause(); //暂停
                break;
            case R.id.btn_stop:
                mPlayer.stop(); //停止
                break;
            case R.id.btn_reset:
                mPlayer.reset(); //重置
                break;
            case R.id.btn_play_pause:
                mPlayer.performPlayPause(); // 播放/暂停
                break;
            case R.id.btn_play_stop:
                mPlayer.performPlayStop(); // 播放/停止
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPlayer.release();
        mLooper.removeCallbacksAndMessages(null);
    }
}
