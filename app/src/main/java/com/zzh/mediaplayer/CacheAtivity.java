package com.zzh.mediaplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.zzh.lib.video.cache.preload.PreMultiLoadManager;
import com.zzh.lib.video.cache.preload.PreloadManager;

import java.util.Set;

public class CacheAtivity extends AppCompatActivity {

    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cache);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page == 1) {
                    PreMultiLoadManager.getInstance(CacheAtivity.this).getOrCreateVideoPreLoadFuture("cache").updateUrls(SetUtils.genSet1());
                } else if (page == 2) {
                    PreMultiLoadManager.getInstance(CacheAtivity.this).getOrCreateVideoPreLoadFuture("cache").addUrls(SetUtils.genSet2());

                } else if (page == 3) {
                    PreMultiLoadManager.getInstance(CacheAtivity.this).getOrCreateVideoPreLoadFuture("cache").addUrls(SetUtils.genSet3());
                } else if (page == 4) {
                    PreMultiLoadManager.getInstance(CacheAtivity.this).getOrCreateVideoPreLoadFuture("cache").addUrls(SetUtils.genSet4());
                } else {
                }
                page++;
            }
        });

        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreMultiLoadManager.getInstance(CacheAtivity.this).getOrCreateVideoPreLoadFuture("cache").currentPlayUrl("http://wxpic.4006188666.com/upimg/parimg/20220513/1652422197949045.mp4");
            }
        });
    }
}