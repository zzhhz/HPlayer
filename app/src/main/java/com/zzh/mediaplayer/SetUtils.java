package com.zzh.mediaplayer;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ZZH on 2022/8/21.
 *
 * @Date: 2022/8/21
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description:
 */
public class SetUtils {
    public static List<String> genSet1() {
        List<String> list = new ArrayList<>(Arrays.asList("http://wxpic.4006188666.com/upimg/parimg/20220429/1651210862171798.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220427/1651049889551275.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220427/1651034308932137.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220518/1652848799535962.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220427/1651033573731450.mp4"));
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220513/1652422197949045.mp4");
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220513/1652417439624903.mp4");
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220513/1652412303761195.mp4");
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220513/1652410991106703.mp4");
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220518/1652848799535962.mp4");
        return list;
    }

    public static List<String> genSet2() {
        List<String> list = new ArrayList<>(Arrays.asList("http://wxpic.4006188666.com/upimg/parimg/20220513/1652422197949045.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220513/1652417439624903.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220513/1652412303761195.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220513/1652410991106703.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220513/1652409354622197.mp4"));
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220513/1652409354622197.mp4");
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220512/1652345822343651.mp4");
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220512/1652336052502439.mp4");
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220512/1652333861390059.mp4");
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220510/1652170657985496.mp4");


        return list;
    }

    public static List<String> genSet3() {
        List<String> list = new ArrayList<>(Arrays.asList("http://wxpic.4006188666.com/upimg/parimg/20220512/1652345822343651.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220512/1652336052502439.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220512/1652333861390059.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220510/1652170657985496.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220427/1651033284611581.mp4"));
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220427/1651051024203441.mp4");
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220429/1651210862171798.mp4");
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220427/1651049889551275.mp4");
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220429/1651209331646626.mp4");
        list.add("http://wxpic.4006188666.com/upimg/parimg/20220427/1651034308932137.mp4");
        return list;
    }

    public static List<String> genSet4() {
        List<String> list = Arrays.asList(
                "http://wxpic.4006188666.com/upimg/parimg/20220429/1651210862171798.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220427/1651049889551275.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220518/1652848799535962.mp4",
                "http://v.4006188666.com/1/1661327265311328.mp4",
                "http://v.4006188666.com/1/1661327234593620.mp4",
                "http://v.4006188666.com/1/1661327200257531.mp4",
                "http://v.4006188666.com/1/1661327174983865.mp4",
                "http://v.4006188666.com/1/1661326986418224.mp4",
                "http://v.4006188666.com/1/1661326952786986.mp4",
                "http://v.4006188666.com/1/1661326914183890.mp4",
                "http://v.4006188666.com/1/1661326881908077.mp4",
                "http://v.4006188666.com/1/1661326828467476.mp4",
                "http://v.4006188666.com/1/1660120356641365.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220429/1651210862171798.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220427/1651049889551275.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220427/1651034308932137.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220518/1652848799535962.mp4",
                "http://v.4006188666.com/1/1660118916569467.mp4",
                "http://v.4006188666.com/1/1659858478966975.mp4",
                "http://v.4006188666.com/1/1659858450566905.mp4",
                "http://v.4006188666.com/1/1659858414594315.mp4",
                "http://v.4006188666.com/1/1659858281745419.mp4",
                "http://v.4006188666.com/1/1659793857449415.mp4",
                "http://zb.4006188666.com/00APP/7039201.mp4",
                "http://zb.4006188666.com/00APP/7039204.mp4",
                "http://zb.4006188666.com/00APP/7039197.mp4",
                "http://zb.4006188666.com/00APP/7039151.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220429/1651210862171798.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220427/1651049889551275.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220427/1651034308932137.mp4",
                "http://wxpic.4006188666.com/upimg/parimg/20220518/1652848799535962.mp4");
        return list;
    }
}
